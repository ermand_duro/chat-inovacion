# Group Chat using Laravel and Pusher

This repository demonstrates the process to create a realtime *Group Chat Application* using [Laravel](https://laravel.com/) and [Pusher](https://pusher.com/). Please follow below steps to have a running version of the app in this repo

1. Clone repo
2. Configure your environment variables for Pusher and Laravel by copying the `.env.example` to `.env`
    ```
    PUSHER_APP_ID
    PUSHER_APP_KEY
    PUSHER_APP_SECRET
    PUSHER_APP_CLUSTER
    ```
3. Configure your Pusher key in the `resources/assets/js/bootstrap.js` file
    ```js
    window.Echo = new Echo({
        broadcaster: 'pusher',
        key: 'pusher-key',
        cluster: 'pusher-cluser',
        encrypted: true
    });
    ```
4. Run `composer install`
5. Run `npm install`
6. Run `npm run dev`
6. Do not forget to run the queue listener to broadcast the events, `php artisan queue:listen`
